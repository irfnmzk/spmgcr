require('dotenv').config()
const TelegramBot = require('node-telegram-bot-api');

const TOKEN = process.env.TELEGRAM_TOKEN || '5678676933:AAE2GQYERQ7S3DcZuIPr0QQiwlYX4WdGwSI';
const options = {
    polling: true
};
const bot = new TelegramBot(TOKEN, options);

const state = {}

const WAGER = 1000

function getBetAmount(userId) {
    const userState = state[userId]

    let currentBet = null; // Preparing variable of bet, for next checking on conditions

    // If Profit is non-negative, return to base
    if (userState.profit >= 0 && userState.rolled > 0) return currentBet // Reset to base bet by putting current bet into null state

    if (userState.baseList.length >= 2) {
        currentBet = (userState.baseList[0] + userState.baseList[userState.baseList.length - 1]) * WAGER;
    } else if (userState.baseList.length === 1) {
        currentBet = (userState.baseList[0] * WAGER) * 2;
    }

    return currentBet;
}

function resetVariables(userId) {
    state[userId] = {
        ...state[userId],
        profit: 0,
        playing: true,
        rolled: 0,
        baseList: [1],
        totalProfit: state[userId].profit > 0 ? state[userId].totalProfit += state[userId].profit : state[userId].totalProfit
    }
}

function gameStart(userId) {
    console.log(state[userId])

    let betAmount = getBetAmount(userId)

    if (!betAmount) {
        resetVariables(userId)
    }

    betAmount = getBetAmount(userId)

    state[userId].totalRolled += 1

    const betText = [
        `*BET #${state[userId].totalRolled}* : ${betAmount}`
    ]

    bot.sendMessage(userId, betText.join('\n'), {
        parse_mode: 'Markdown', reply_markup: {
            inline_keyboard: [
                [
                    {
                        text: 'Win',
                        callback_data: 'win'
                    },
                    {
                        text: 'Lose',
                        callback_data: 'lose'
                    },
                    {
                        text: 'Stop',
                        callback_data: 'stop'
                    },
                ]
            ]
        }
    })
}

async function gameWin(userId) {
    const lastBet = getBetAmount(userId)

    const { baseList } = state[userId]

    if (baseList.length > 1) baseList.splice(baseList.length - 1, 1)
    baseList.splice(0, 1);

    state[userId].profit += Math.round((lastBet * (2 - 1)))
    state[userId].balance += Math.round((lastBet * (2 - 1)))
    state[userId].rolled += 1

    const { balance } = state[userId]

    await bot.sendMessage(userId, `*BET WIN* : +${Math.round((lastBet * (2 - 1)))}\n*Balance* : ${balance}`, { parse_mode: 'Markdown' })

    return gameStart(userId)

}

async function gameLose(userId) {
    const lastBet = getBetAmount(userId)

    state[userId].baseList.push(lastBet / WAGER)
    state[userId].profit -= (lastBet)
    state[userId].balance -= (lastBet)
    state[userId].rolled += 1

    const { balance } = state[userId]

    await bot.sendMessage(userId, `*BET LOST* : -${lastBet} \n*Balance* : ${balance}`, { parse_mode: 'Markdown' })

    return gameStart(userId)
}

async function gameStop(userId) {
    const { balance, totalRolled, totalProfit } = state[userId]

    const text = [
        `*Balance* : ${balance}`,
        `*Total Games* : ${totalRolled}`,
        `*Total Profit* : ${totalProfit}`
    ]

    delete state[userId]

    await bot.sendMessage(userId, text.join('\n'), { parse_mode: 'Markdown' })
}

bot.onText(/\/start (\d+)/, (msg, match) => {
    const startBalance = match[1];

    const defaultState = {
        payout: 2,
        balance: parseInt(startBalance),
        totalProfit: 0,
        totalRolled: 0,
        profit: 0,
        playing: true,
        rolled: 0,
        baseList: [1]
    }

    state[msg.chat.id] = defaultState


    const userState = state[msg.chat.id]
    const text = [
        'Spaceman gacor di mulai!!',
        `*Balance* : ${userState.balance}`,
    ]

    bot.sendMessage(msg.chat.id, text.join('\n'), { parse_mode: 'Markdown' })

    return gameStart(msg.chat.id)
})

bot.on('callback_query', callback => {
    const action = callback.data

    if (!state[callback.from.id]) {
        return
    }

    switch (action) {
        case 'win':
            return gameWin(callback.from.id)
        case 'lose':
            return gameLose(callback.from.id)
        case 'stop':
            return gameStop(callback.from.id)
        default:
            return
    }
})

bot.onText(/^\/start$/, (msg) => {
    bot.sendMessage(msg.chat.id, 'Untuk mulai silahkan ketik /start <saldo awal> \ncontoh /start 100000')
})